<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="model.User,model.Mutter,java.util.List"%>
<%@ page import="java.util.Calendar"%>
<%
	//セッションスコープに保存されたユーザー情報を取得
	User loginUser = (User) session.getAttribute("loginUser");
	//アプリケーションスコープに保存されたつぶやきリストを取得
	List<Mutter> mutterList = (List<Mutter>) application.getAttribute("mutterList");
	//リクエストスコープに保存されたエラーメッセージを取得
	String errorMsg = (String) request.getAttribute("errorMsg");
%>

<%
Calendar cal = Calendar.getInstance();
int year = cal.get(Calendar.YEAR);
int month = cal.get(Calendar.MONTH) + 1;
int day = cal.get(Calendar.DATE);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="http://localhost:8080/projectC/css/index.css">
<title>日記帳</title>
</head>
<body>

<div class="text">
	<h1>日記帳メイン</h1>
	<p>
		<%=loginUser.getName()%>さん、ログイン中
		<a href="/projectC/Logout">ログアウト</a>
	</p>
	<p><a href="/projectC/Main">更新</a></p>

<table border="0" cellspacing="1" cellpadding="1" bgcolor="#99FF00" style="font: 12px; color: #666666;">
<tr>
<td align="center" colspan="7" bgcolor="#99FF00" height="18" style="color: #000000;">2019年9月</td></tr>
<tr>
<td align="center" width="20" height="18" bgcolor="#009900" style="color: #FFFFFF;">日</td>
<td align="center" width="20" bgcolor="#99FF66" style="color: #666666;">月</td>
<td align="center" width="20" bgcolor="#99FF66" style="color: #666666;">火</td>
<td align="center" width="20" bgcolor="#99FF66" style="color: #666666;">水</td>
<td align="center" width="20" bgcolor="#99FF66" style="color: #666666;">木</td>
<td align="center" width="20" bgcolor="#99FF66" style="color: #666666;">金</td>
<td align="center" width="20" bgcolor="#00CC66" style="color: #666666;">土</td>
</tr>
<tr>
<td align="center" height="18" bgcolor="#99FFFF" style="color: #666666;">1</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">2</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">3</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">4</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">5</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">6</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">7</td>
</tr>
<tr>
<td align="center" height="18" bgcolor="#99FFFF" style="color: #666666;">8</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">9</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">10</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">11</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">12</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">13</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">14</td>
</tr>
<tr>
<td align="center" height="18" bgcolor="#99FFFF" style="color: #666666;">15</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">16</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">17</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">18</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">19</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">20</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">21</td>
</tr>
<tr>
<td align="center" height="18" bgcolor="#99FFFF" style="color: #666666;">22</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">23</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">24</td>
<td align="center" bgcolor="#006600" style="color: #FFFFFF;">25</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">26</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">27</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">28</td>
</tr>
<tr>
<td align="center" height="18" bgcolor="#99FFFF" style="color: #666666;">29</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">30</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">　</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">　</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">　</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">　</td>
<td align="center" bgcolor="#FFFFFF" style="color: #666666;">　</td>
</tr>
<tr>
<td bgcolor="#FFFFFF" height="18">&nbsp;</td>
<td bgcolor="#FFFFFF">&nbsp;</td>
<td bgcolor="#FFFFFF">&nbsp;</td>
<td bgcolor="#FFFFFF">&nbsp;</td>
<td bgcolor="#FFFFFF">&nbsp;</td>
<td bgcolor="#FFFFFF">&nbsp;</td>
<td bgcolor="#FFFFFF">&nbsp;</td>
</tr>
</table>


	<form action="/projectC/Main" method="post">
		<textarea wrap="hard" name="text" rows="8" cols="40"></textarea>
		<input type="submit" value="投稿">
	</form>

<div id="article">
	<% if(errorMsg !=null) { %>
	<p><%= errorMsg %></p>
	<% } %>
	<% for(Mutter mutter:mutterList){ %>
	<p><%--<%= mutter.getUserName() --%><%=year%>年<%=month%>月<%=day%>日:<pre><%= mutter.getText() %></pre></p>
	<% } %>
</div>
</div>
</body>
</html>