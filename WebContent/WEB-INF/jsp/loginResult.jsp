<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="model.User" %>
<%
//セッションスコープからユーザー情報を取得
User loginUser=(User) session.getAttribute("loginUser");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="http://localhost:8080/projectC/css/index.css">
<title>日記帳</title>
</head>
<body>
<div class="text">
<% if(loginUser !=null) { %>
	<h2>ログインに成功しました</h2>
	<p>ようこそ<%= loginUser.getName() %>さん</p>
	<p><a href="/projectC/Main">執筆画面へ</a></p>
	<p><a href="/projectC/Setting">設定画面へ</a></p>
<% }else { %>
	<h2>ログインに失敗しました</h2>
	<a href="/projectC/">TOPへ</a>
<% } %>
</div>
</body>
</html>